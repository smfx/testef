﻿using DispatcherApp.DAL;
using DispatcherApp.Models;
using System.Data.Entity;
using System;


namespace ConsoleDispatcherApp
{
    class consoleDispatcherApp
    {
        static void Main(string[] args)
        {
            using (DispatcherContext db = new DispatcherContext())
            {
                var busList = db.Buses.Include(b => b.Routes);

                foreach (Bus b in busList)
                {
                    Console.WriteLine(b.Model);
                    
                    foreach (Route r in b.Routes)
                    {
                        Console.WriteLine(r.Name);
                    }
                }
            }
        }
    }
}
