﻿using DispatcherApp.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DispatcherApp.DAL
{
    public class DispatcherContext : DbContext
    {
        public DispatcherContext() : base("DispatcherContext") { }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bus>()
                .HasMany<Route>(b => b.Routes)
                .WithMany(r => r.Buses);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Bus> Buses { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Route> Routes { get; set; }
    }
}
