﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using DispatcherApp.Models;

namespace DispatcherApp.DAL
{
    class DispatcherSerializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<DispatcherContext>
    {
        protected override void Seed(DispatcherContext context)
        {
            var buses = new List<Bus>
            {
                new Bus{ Id=1, Model="Volvo", Year=1993 },
                new Bus{ Id=2, Model="Mercedes", Year=1991 },
                new Bus{ Id=3, Model="Daihatsy", Year=2008 },
                new Bus{ Id=4, Model="Icarus", Year=2001 },
                new Bus{ Id=5, Model="Fiat", Year=2004 }
            };
            buses.ForEach( s => context.Buses.Add(s) );
            context.SaveChanges();

            var drivers = new List<Driver>
            {
                new Driver{ FirstName="John", LastName="Smith", BusId = 1 },
                new Driver{ FirstName="Din", LastName="Parcell", BusId = 2 },
                new Driver{ FirstName="Adam", LastName="Smith", BusId = 1 },
            };
            drivers.ForEach(s => context.Drivers.Add(s));
            context.SaveChanges();

            var routes = new List<Route>
            {
                new Route{ Id = 1, Name="Montre", Buses = new List<Bus>{buses[0], buses[3] } },
                new Route{ Id = 2, Name="Lyon", Buses = new List<Bus>{buses[1], buses[2], buses[3] } },
                new Route{ Id = 3, Name="Geneve", Buses = new List<Bus>{buses[0], buses[4] } }

            };
            routes.ForEach(s => context.Routes.Add(s));
            context.SaveChanges();

        }
    }
}
