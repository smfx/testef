﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatcherApp.Models
{
    public class BusRoute
    {
        public int BusId { get; set; }
        public virtual Bus Bus { get; set; }
        public int RouteId { get; set; }
        public virtual Route Route { get; set; }
    }
}
