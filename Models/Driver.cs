﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatcherApp.Models
{
    public class Driver
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int BusId { get; set; }
        public virtual Bus Bus { get; set; }
    }
}
