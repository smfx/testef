﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatcherApp.Models
{
    public class Bus
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public ICollection<Route> Routes { get; set; }

        public Bus()
        {
            Routes = new List<Route>();
        }
    }
}
