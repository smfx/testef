﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatcherApp.Models
{
    public class Route
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Bus> Buses { get; set; }
        public Route()
        {
            Buses = new List<Bus>();
        }
    }
}
